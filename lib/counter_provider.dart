import 'package:flutter/material.dart';

class CounterProvider with  ChangeNotifier {
  CounterProvider._privateConstructor();

  static final CounterProvider _instance = CounterProvider._privateConstructor();

  static CounterProvider get instance => _instance;

  int _counter = 0;

  int get counter{
    return _counter;
  }

  void addCounter(){
    _counter++;
    notifyListeners();
}
  void removeCounter(){
    _counter--;
    notifyListeners();
  }
}